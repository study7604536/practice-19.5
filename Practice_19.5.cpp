﻿
#include <iostream>
#include "cstring"
using namespace std;

class Animal
{
private:
    string phrase="Lets hear the voice of my children\n";
public:
    virtual void Voice()
    {
        cout << phrase;
    }

};

class Dog: public Animal
{
private:

    string phrase = "Dog says Woof!!\n";

public:
    
    void Voice() override
    {
        cout << phrase;
    }

};

class Cat : public Animal
{
private:

    string phrase = "Cat says Meow!!\n";

public:

    void Voice() override
    {
        cout << phrase;
    }

};

class Fox : public Animal
{
private:

    string phrase = "What does the fox says???\n";

public:

    void Voice() override
    {
        cout << phrase;
    }

};

class Bunny : public Animal
{
private:

    string phrase = "Bunny says: it wasn't tax fraud it was tax evasion\n"; // Плохой зайчик

public:

    void Voice() override
    {
        cout << phrase;
    }

};


int main()
{
    Animal say1;
    Dog say2;
    Cat say3;
    Fox say4;
    Bunny say5;

    Animal* chorus[] = { &say1,&say2,&say3,&say4,&say5 };

    for (size_t i=0; i < size(chorus); ++i)
    {
        chorus[i]->Voice();
    }
}
